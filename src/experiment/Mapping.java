package experiment;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/** 
 * An experimenting class which makes an ArrayList of [i]dataAmount[/i] fictional student grades between 3 and 10,
 * and will then calculate the average. First with a lambda expression and then without. 
 * The time it takes is calculated and presented in the console.
 * 
 * @author Chris
 */
public class Mapping {
	
	// Amount of data
	private static final int dataAmount = 25000000;
	
	// The data that will be used for mapping in this experiment
	private ArrayList<Double> data = new ArrayList<Double>();
	
	public Mapping() {
		Random random = new Random();
		
		// Adding dataAmount random numbers between 3 and 10
		for (int i = 0; i < dataAmount; i++) {
			data.add(3 + (random.nextInt(70) / 10.0));
		}
	}
	
	private static double gradeNumberToPercentage(double gradeNumber) {
		double part = gradeNumber - 1;
		double total = 9;
		
		return part * 100 / total;
	}
	
	private static char gradeNumberToLetter(double number) {
		double percentage = gradeNumberToPercentage(number);
		
		if (percentage >= 90) {
			return 'A';
		} else if (percentage >= 80) {
			return 'B';
		} else if (percentage >= 70) {
			return 'C';
		} else if (percentage >= 60) {
			return 'D';
		}
		return 'F';
	}
	
	private void mapRegular() {
		// Using mapping to convert from number grades to letter grades
		List<Character> mapped = new ArrayList<Character>();
		for (double value : data) {
			mapped.add(gradeNumberToLetter(value));
		}
		
		// Validate behavior
		System.out.println(data.get(50));
		System.out.println(mapped.get(50));
	}
	
	private void mapLambda() {
		// Using mapping to convert from number grades to letter grades, using a lambda expression
		List<Character> mapped = data
				.stream()
				.map(x -> gradeNumberToLetter(x))
				.collect(Collectors.toList());

		// Validate behavior
		System.out.println(data.get(50));
		System.out.println(mapped.get(50));
	}
	
	public void execute(boolean useLambda) {
		// Save the time just before mapping
		long startTime = System.currentTimeMillis();
		
		// Use the defined method
		if (useLambda) {
			mapLambda();
		} else {
			mapRegular();
		}
		
		// Save the time just after mapping 
		long endTime = System.currentTimeMillis();
		
		// Calculate how long it took.
		long timeTaken = endTime - startTime;
		
		// Print the time in miliseconds
		System.out.println("Mapping with" + (useLambda? "": "out") + " lambda took: " + timeTaken + " ms.");
	}
	
	public static void main(String[] args) {
		// First make this object. Constructor builds test-data
		System.out.println("Making object and building test-data...");
		Mapping mapping = new Mapping();
		System.out.println("Object made. Ready to execute experiment.");
	
		// Execute with and without lambda
		mapping.execute(true);
		mapping.execute(false);
	
	}
}
