package experiment;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * An experimenting class which makes an ArrayList of [i]dataAmount[/i] fictional student numbers between 250.000 and 350.000,
 * and will then get all the numbers above 300.000 using filtering. First with a lambda expression and then without. 
 * The time it takes is calculated and presented in the console.
 * 
 * @author Chris
 */
public class Filtering {
	
	// Amount of data
	private static final int dataAmount = 25_000_000;
	
	// The data that will be filtered in this experiment
	private ArrayList<Integer> data = new ArrayList<Integer>();
	
	public Filtering() {
		Random random = new Random();
		
		// Adding dataAmount fictional numbers between 250000 and 350000 to the data object.
		for (int i = 0; i < dataAmount; i++) {
			data.add(250000 + random.nextInt(100000));
		}
	}
	
	private void filterRegular() {
		// Filtering all numbers below 300.000 out using a for-loop and if-statement
		List<Integer> filtered = new ArrayList<Integer>();
		for (int value : data) {
			 if (value > 300_000) {
				 filtered.add(value);
			 } 
		}
		
		// Validate behavior
		System.out.println("Numbers left after filtering: " + filtered.size());
	}
	
	private void filterLambda() {
		// Filtering all numbers below 300.000 out using a lambda expression
		List<Integer> filtered = data
				.stream()
				.filter(x -> x.intValue() > 300_000)
				.collect(Collectors.toList());
		
		// Validate behavior
		System.out.println("Numbers left after filtering: " + filtered.size());
	}
	
	public void execute(boolean useLambda) {
		// Save the time just before filtering
		long startTime = System.currentTimeMillis();
		
		// Use the defined method
		if (useLambda) {
			filterLambda();
		} else {
			filterRegular();
		}
		
		// Save the time just after filtering 
		long endTime = System.currentTimeMillis();
		
		// Calculate how long it took.
		long timeTaken = endTime - startTime;
		
		// Print the time in miliseconds
		System.out.println("Filtering with" + (useLambda? "": "out") + " lambda took: " + timeTaken + " ms.");
	}
	
	public static void main(String[] args) {
		// First make this object. Constructor builds test-data
		System.out.println("Making object and building test-data...");
		Filtering filtering = new Filtering();
		System.out.println("Object made. Ready to execute experiment.");
		
		// Execute with and without lambda
		filtering.execute(true);
		filtering.execute(false);
	}
}
