package experiment;
import java.util.ArrayList;
import java.util.Random;

/** 
 * An experimenting class which makes an ArrayList of [i]dataAmount[/i] fictional student grades between 3 and 10,
 * and will then calculate the average. First with a lambda expression and then without. 
 * The time it takes is calculated and presented in the console.
 * 
 * @author Chris
 */
public class Reduction {

	// Amount of data
	private static final int dataAmount = 25_000_000; // 4_103_000 = tijd *10..? 
	
	// The data that will be reduced in this experiment
	private ArrayList<Double> data = new ArrayList<Double>();
	
	public Reduction() {
		Random random = new Random();
		
		// Adding dataAmount random numbers between 3 and 10
		for (int i = 0; i < dataAmount; i++) {
			data.add(3 + (random.nextInt(70) / 10.0));
		}
	}
	
	private void reduceRegular() { 
		// Reducing the set of numbers into one number which represents the average.
		Double sum = 0.0;
		int numbers = data.size();

		for (Double value : data) 
			 sum += value;
		
		Double average = sum / numbers;
		
		// Validate behavior
		System.out.println("Average: " + average);
	}
	
	private void reduceLambda() {
		// Reducing the set of numbers into one number which represents the average.
		Double reduced = data
				.stream()
				.mapToDouble(x -> x)
				.average()
				.getAsDouble();
		
		// Validate behavior
		System.out.println("Average: " + reduced);
	}
	
	public void execute(boolean useLambda) {
		// Save the time just before reducing
		long startTime = System.currentTimeMillis();
		
		// Use the defined method
		if (useLambda) {
			reduceLambda();
		} else {
			reduceRegular();
		}
		
		// Save the time just after reducing 
		long endTime = System.currentTimeMillis();
		
		// Calculate how long it took.
		long timeTaken = endTime - startTime;
		
		// Print the time in milliseconds
		System.out.println("Reduction with" + (useLambda? "": "out") + " lambda took: " + timeTaken + " ms.");
	}
	
	public static void main(String[] args) {
		// First make this object. Constructor builds test-data
		System.out.println("Making object and building test-data...");
		Reduction reduction = new Reduction();
		System.out.println("Object made. Ready to execute experiment.");
		
		// Execute with and without lambda
		reduction.execute(true);
		reduction.execute(false);
	}
}
